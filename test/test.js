const request = require('supertest');
const app = require('../app');
let server;

beforeAll(() => {
  server = require('../index');  // Ensure the server is started for testing
});

afterAll((done) => {
  server.close(done);  // Ensure the server is closed after tests
});

describe('CRUD Operations', () => {
  let itemId;

  beforeAll(async () => {
    const res = await request(app)
      .post('/items')
      .send({ id: 1, name: 'Test Item' });
    itemId = res.body.id;
  });

  test('should create a new item', async () => {
    const res = await request(app)
      .post('/items')
      .send({ id: 2, name: 'New Item' });

    expect(res.status).toBe(201);
    expect(res.body).toHaveProperty('id', 2);
  });

  test('should get all items', async () => {
    const res = await request(app).get('/items');

    expect(res.status).toBe(200);
    expect(res.body).toBeInstanceOf(Array);
  });
});