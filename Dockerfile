# Use the official Node.js image with Alpine Linux
FROM node:20.9.0-alpine

# Create and change to the app directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the application port (if needed)
# EXPOSE 3001

# Start the application
CMD ["npm", "start"]