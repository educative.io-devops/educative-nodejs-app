const express = require('express');
const app = express();

app.use(express.json());

// Initialize items array with mock data
let items = [
  { id: 1, name: 'Item 1', description: 'Description of Item 1' },
  { id: 2, name: 'Item 2', description: 'Description of Item 2' },
  { id: 3, name: 'Item 3', description: 'Description of Item 3' },
];

// Default route
app.get('/', (req, res) => {
  res.send('Welcome to Educative.io Node.js App');
});

// Create
app.post('/items', (req, res) => {
  const item = { id: items.length + 1, ...req.body };
  items.push(item);
  res.status(201).send(item);
});

// Read
app.get('/items', (req, res) => {
  res.status(200).send(items);
});

// Update
app.put('/items/:id', (req, res) => {
  let item = items.find(i => i.id === parseInt(req.params.id));
  if (!item) return res.status(404).send('Item not found.');
  item.name = req.body.name;
  item.description = req.body.description; // Update the description as well
  res.send(item);
});

// Delete
app.delete('/items/:id', (req, res) => {
  items = items.filter(i => i.id !== parseInt(req.params.id));
  res.status(204).send();
});

module.exports = app;